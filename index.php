<!DOCTYPE html">
<html>
	<head>
	<meta charset="utf-8">
	<title>Car Dealer | Registracija</title>	
	<meta name="description" content="Dobro došli na našu web stranicu. Ovde možete pogledati našu ponudu novih i polovnih automobila različitih proizvođača.">
	<meta name="keywords" content="#">
	<meta name="author" content="Bojan">
	<meta name="viewport" content="width=device-width, initial-scale=1.O">	
	<link href="https://fonts.googleapis.com/css?family=Kalam:300,400,700|Oswald:300,400,500,600,700|Roboto+Condensed:300,400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	</head>	
	<body>
		<div class="wrapper">
				
			<!--CONTAINER-->
			<div class="container">
							
				<!--REGISTRACIJA-->
				<div class="registracija">
					<div class="title">
					<h2>Registracija</h2>		
					<p>Registraciona prijava</p>	
					</div>	

<?php
    $name = $folder = $file = "";
    $nameErr = $folderErr = $fileErr = "";
    
    if (isset($_GET['name'])) { $name = $_GET['name']; }
    if (isset($_GET['folder'])) { $folder = $_GET['folder']; }
	if (isset($_GET['file'])) { $file = $_GET['file']; }

    if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
    if (isset($_GET['folderErr'])) { $folderErr = $_GET['folderErr']; }
	if (isset($_GET['fileErr'])) { $fileErr = $_GET['fileErr']; }
    
?>
<div class="registracija-box">
<form method="post" name="upload" action="myPhoto.php" enctype="multipart/form-data" >

	
	<h4>Sva polja označena sa <span>*</span> morate obavezno da popunite</h4>
	
	<p>Ime <span>*  <?php echo $nameErr;?></span></p>
	<input type="text" name="name" value="<?php echo $name;?>">
      
	<p>Folder <span>*  <?php echo $folderErr;?></span></p>
  <p><input class="checkbox" type="radio" name="folder" <?php if (isset($folder) && $folder=="private") echo "checked";?> value="private">Private</p>
  <p><input class="checkbox" type="radio" name="folder" <?php if (isset($folder) && $folder=="public") echo "checked";?> value="public">Public</p>
  
	<p>Slika <span>*  <?php echo $fileErr;?></span></p>
	<p><input type="file" name="file" id="if" value="<?php echo $file;?>"/></p>
  
  <button type="submit" name="sb" id="sb" value="upload"> Upload</button>
</form>

</div>
				</div>	
				<!--KRAJ REGISTRACIJE-->
				
				
			</div>

			
		</div>
	</body>
</html>