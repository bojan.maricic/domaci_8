<!DOCTYPE html">
<html>
	<head>
	<meta charset="utf-8">
	<title>Registracija</title>		
	<link href="https://fonts.googleapis.com/css?family=Kalam:300,400,700|Oswald:300,400,500,600,700|Roboto+Condensed:300,400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	</head>	
	<body>
		<div class="wrapper">			
			<!--CONTAINER-->
			<div class="container">
				<div class="registracija">
					<div class="title">
						<h2>Registracija</h2>		
						<p>Registraciona prijava</p>	
					</div>	
					<div class="registracija-box">
						<h4>Vaši podaci iz registracione prijave su:</h4>  
						


<?php

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

// define variables and set to empty values
$name = $folder = $file = "";
$nameErr = $folderErr = $fileErr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Unesi ime";
  } else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed"; 
    }
	if (strlen($name) < 3){
	$nameErr = "Ime mora imati više od 2 karaktera";
	}
  }

  if (empty($_POST["folder"])) {
    $folderErr = "Izaberi folder";
  } else {
    $folder = test_input($_POST["folder"]);
  }
  
if (!empty($nameErr) or !empty($folderErr) or !empty($fileErr)) {
    $params = "name=" . urlencode($_POST["name"]);
	$params = "&folder=" . urlencode($_POST["folder"]);
    $params .= "&file=" . urlencode($_POST["file"]);
   

    $params .= "&nameErr=" . urlencode($nameErr);
	$params .= "&folderErr=" . urlencode($folderErr);
    $params .= "&fileErr=" . urlencode($fileErr);
    
    header("Location: index.php?" . $params);
  }  else {

  }  
  
}


if(isset($_FILES["file"]) AND is_uploaded_file($_FILES['file']['tmp_name'])) {

    $file_name = $_FILES['file']["name"];
    $file_temp = $_FILES["file"]["tmp_name"];
    $file_size = $_FILES["file"]["size"];
    $file_type = $_FILES["file"]["type"];
    $file_error = $_FILES['file']["error"];

	 if ($file_error >0) {
      echo "Something went wrong during file upload!";
    } else {
        // http://en.wikipedia.org/wiki/Exchangeable_image_file_format
        // http://www.php.net/manual/en/book.exif.php
        echo "Image type: ".exif_imagetype($file_temp)."<br />";

        if (exif_imagetype($file_temp)!=2) {
          exit("Image is not a jpg");
        }
      }

	  
	echo "Ime poslate datoteke: $file_name <br />";
    echo "Privremena lokacija datoteke: $file_temp <br />";
    echo "Veličina poslate datoteke u bajtovima: $file_size <br />";
    echo "Tip poslate datoteke: $file_type <br />";
    echo "Kod greške: $file_error <br />";
 
	
	
	$ext_temp = explode(".", $file_name);
    $extension = end($ext_temp);
	$rand = rand(1,10);
	$date = Date("Ymd");
	$file_name_name = "$name";
	$new_file_name = "$name" . "_" . Date("Ymd") . "_" . "$rand" . ".$extension";
        
  
     $dir = $_POST['folder'];
     $upload = "$dir/$new_file_name";


        // upload fajla
        if (!is_dir($dir)) {
          mkdir($dir);
        }

        if (!file_exists($upload)) {

            if (move_uploaded_file($file_temp, $upload)) {
                $size = getimagesize($upload);
                //var_dump($size);
                foreach ($size as $key => $value)
                echo "$key = $value<br />";

                /*echo "<img src=\"$upload\" $size[3] border=\"0\" alt=\"$file_name\" />";*/

            }
            else {
              echo "<p><b>Error!</b></p>";
            }

        }
        else {
            for($i=1; $i<9; $i++){

                if(!file_exists($dir.'/'.$file_name_name.$i.'.'.$extension)){

                  move_uploaded_file($file_temp, $dir.'/'.$file_name_name.$i.'.'.$extension);
                  break;

                }
                /*else{

                  echo "<p><b>File with this name already exists!</b></p>";

                }*/

            }
        }
    }

?>


					</div>
				</div>	
			</div>
	</body>
</html>
